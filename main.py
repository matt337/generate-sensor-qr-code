from pathlib import Path

import qrcode
from qrcode.image.pil import PilImage
import csv


class InvalidVendorCodeError(Exception):
    pass


class InvalidOutputDirectoryError(Exception):
    pass


class GenerateSensorQRCodeFromCSV:
    """Generate QR codes that mimics the format used by various sensor vendors.

    Takes a CSV file as input containing the sensor identifiers and vendor codes.

    Note only a few vendors are currently supported.

    Overwrites image files by default if they already exist.

    """

    CSV_RELATIVE_FILE_PATH = "qr_codes_to_generate.csv"
    OUTPUT_DIR_RELATIVE_FILE_PATH = "qr_codes"
    QR_CODE_REGEX = {
        "BEA": "www.sensorio.com/#/qr/{0}",
        "DTE": "{0}",
        "AIR": "{0}"
    }

    @classmethod
    def generate(cls) -> None:
        with open(cls.CSV_RELATIVE_FILE_PATH) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            line_count = 0
            output_dir = Path(Path(__file__).parent, cls.OUTPUT_DIR_RELATIVE_FILE_PATH)

            for row in csv_reader:
                vendor_formatted_input = cls._format_vendor_pattern(row["vendor_code"], row["sensor_identifier"])
                qr_code_image = cls._generate_qr_code(vendor_formatted_input)
                output_file = Path(output_dir, f"{row['sensor_identifier']}.png")
                try:
                    qr_code_image.save(output_file)
                except FileNotFoundError:
                    raise InvalidOutputDirectoryError("Relative output directory not found!")
                line_count += 1

            print(f"Generated {line_count} QR codes in: {output_dir}")

    @staticmethod
    def _generate_qr_code(qr_input: str) -> PilImage:
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=2,
        )
        qr.add_data(qr_input)
        qr.make(fit=True)
        return qr.make_image(fill_color="black", back_color="white")

    @classmethod
    def _format_vendor_pattern(cls, vendor_code: str, input_value: str) -> str:
        vendor_format_template = cls.QR_CODE_REGEX.get(vendor_code)
        if not vendor_format_template:
            raise InvalidVendorCodeError(f"Vendor code {vendor_code} not supported")
        return vendor_format_template.format(input_value)


if __name__ == '__main__':
    GenerateSensorQRCodeFromCSV.generate()
